"use strict";

/**
 * instituicao service
 */

const { createCoreService } = require("@strapi/strapi").factories;

module.exports = createCoreService(
  "api::instituicao.instituicao",
  ({ strapi }) => ({
    async getInstituicaoByNomeService(nome) {
      try {
        console.log("nome = ", nome);
        let result = await strapi.db
          .query("api::instituicao.instituicao")
          .findOne({
            where: { nome: nome },
          });
        const departments = await strapi
          .service("api::departamento.departamento")
          .getDepartamentsByInstitute(nome);
        let resultDepartments = {};
        departments.forEach((d) => {
          // console.(d);
          resultDepartments[d.key] = {
            name: d.name,
            color: d.color,
            chefe: d.chefe,
            viceChefe: d.viceChefe,
            history: d.history,
          };
        });
        result = { ...result, departments: resultDepartments };
        console.log(result);
        return result;
      } catch (err) {
        ctx.body = err;
      }
    },
  })
);
